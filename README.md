## 使用方法

- 把插件目录直接扔到 WHMCS/modules/servers 下即可
- 自定义字段 cloudinit (文本框 textarea 在订单页面显示) pem (文本框 textarea 仅管理员可见)

## 特性

- 动态 IP (关机再开机即可更换 IP)
- 支持用户自选系统
- 支持多账号
- 不支持流量限制 (可定制)

## 赞助

- USDT TRC20 地址 `TNU2wK4yieGCWUxezgpZhwMHmLnRnXRtmu`
